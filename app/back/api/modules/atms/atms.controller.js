'use strict' 

var  request = require('request');

/*
 |--------------------------------------------------------------------------
 | Retrieve a list of atms in Spain
 |--------------------------------------------------------------------------
 */
exports.atms = function(req, res){

    var lat = req.swagger.params.latitude.value;
    var lng = req.swagger.params.longitude.value;
    var radius = req.swagger.params.radius.value;

    var atmsUrl = process.env.BBVA_API_URL + process.env.BBVA_API_ATM_PATH;

    //the user has all the active providers. I need the bbva provider
    //to get the token. If the bbva provider is not enable, return an error
    var bbvaProviderType = 'BBVA';
    var providers = req.user.providers;
    var bbvaProvider;
    var token;

    if(providers){
        for(var i = 0; i< providers.length ; i++){
            if (providers[i].providerType === bbvaProviderType){ //found it
                bbvaProvider = providers[i];
            }
        }
        if(!bbvaProvider){
            return res.status(500).send({message: 'The user has not the BBVA Provider'});
        }
        
        if(!bbvaProvider.enabled){
            return res.status(500).send({message: 'The BBVA Provider is disabled for the user'});
        }

        token = bbvaProvider.accessToken;
     
        if(!token){
            return res.status(500).send({message: 'The user has not BBVA Provider access token'});   
        }

        radius = radius ? ('&radius='+radius):'';
        var options = {
            url: atmsUrl + '?longitude='+lng+'&latitude='+lat+radius ,
            headers : {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization': 'jwt ' + token
           },   
        }
     
         
         // Step 1. Exchange authorization code for access token.
        request.get(options, function(err, response, body) {

            if(err) return next(new Error({ message: err.message }));
           
            var json = JSON.parse(body);
           
            var atms = json.data.atms;
            
            //The api return latitude and longitude as string. We need it as float
            for(var i = 0; i < atms.length; i++){
                var geo = atms[i].geolocation;

                geo.longitude = parseFloat(geo.longitude);
                geo.latitude = parseFloat(geo.latitude)
            }

            atms.sort(compare);
            console.log('ATMS: ' + JSON.stringify(atms));            
            res.send(atms);
         }); 
    }else{
        res.status(500).send({message: "The user has not providers"});
    }
}

function compare(a,b) {
    if (a.distance < b.distance)
      return -1;
    if (a.distance > b.distance)
      return 1;
    return 0;
  }
  