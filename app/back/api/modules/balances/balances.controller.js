'use strict'

var request = require('request');
var providerController = require('../provider/provider.controller');
var _ = require('lodash')

/*
 |--------------------------------------------------------------------------
 | Retrieve all balances of the logged user
 |--------------------------------------------------------------------------
 */
exports.balances = function (req, resp) {
    //buscar los providers del user
  let user = req.user;
  var arr = user.providers;
  var promises=[];
  

  arr.forEach(function(element) {
    if(element.enabled){
      promises.push(balance(element).then(
        (res) => {return res}));
    }
  });
  return Promise.all(promises).then((balances) => {

    var totals = [];
    for(var i in balances){
      for(var j in balances[i].totals){
        totals.push(balances[i].totals[j]);
      }
    }
    var currencies = _.groupBy(totals, 'currency');
    var t = [];
    for(var key in currencies){
      var o = {
        currency: key,
        amount: 0
      }
      for(var i in currencies[key]){
        o.amount = o.amount + currencies[key][i].amount
      }
      t.push(o);
    }
    var result = {
      totals : t,
      accounts: balances
    }
    resp.send(result) ;
  }).catch(function (e) {
    console.log("Promise Rejected");
    throw new Error (e.message);
  });

};




function balance (element){

  return new Promise(resolve => {
    let providerType = element.providerType
    let providerStrategy = providerController.selectProviderStrategy(providerType);
    providerStrategy.getBalance(element.providerUserId, element.accessToken,  function(err, result){
      if(err){
        console.log(err.message);
        throw new Error(err.message);
      }
      var balance = {
        name: providerStrategy.getDescription(),
        totals: result.totals, 
        accounts: result.accounts
      };
      resolve(balance);
    });
  });

}