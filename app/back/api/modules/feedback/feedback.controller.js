var mongoose = require('mongoose'),
    faceController = require('../face/face.controller'),
    faceSchema = require('./feedback.model'),    
    Feedback = mongoose.model('Feedback'),
    request = require('request'),
    util = require('util');


exports.feedback = function(req, res){
    faceController.faceAttributesAzureRequest(req, res, function(err, result){
        if (err) {
            return res.status(500).send({ message: err.message });
        }
                
        var feedback = new Feedback({
            comment : req.swagger.params.comment ? req.swagger.params.comment.value : '',
            faceAttributes: result.faceAttributes
        });


        feedback.save(feedback, function(err){
            if (err) {
                return res.status(500).send({ message: err.message });
            }
          
            res.send({message:''  + (feedback.faceAttributes.emotion.happiness*10)});
        });
    });
}

exports.statistics = function(req, res){

    let statistics = {
        clientsGeneration: {},
        generationGender: {},
        clientsGender: {},
        genderGenerationHappiness: {}
    }

    Feedback.aggregate(
        [
           {
              $project: {
                 _id: 0,
                 generation: { $concat: [
                          { $cond: [{$and:[ {$gt:["$faceAttributes.age", 9 ]}, {$lte: ["$faceAttributes.age", 17]}]}, "Generacion Z", ""] },
                          { $cond: [{$and:[ {$gt:["$faceAttributes.age",17]}, {$lte:["$faceAttributes.age", 27]}]}, "Milenials", ""]},
                          { $cond: [{$and:[ {$gt:["$faceAttributes.age",27]}, {$lte:["$faceAttributes.age", 37]}]}, "Generacion Y", ""]},
                          { $cond: [{$and:[ {$gt:["$faceAttributes.age",37]}, {$lt:["$faceAttributes.age", 52]}]}, "Generacion X", ""]},
                          { $cond: [{$and:[ {$gt:["$faceAttributes.age",52]}]}, "Baby Boomer", ""]}]},
                 hapiness: "$faceAttributes.emotion.happiness",
             gender: "$faceAttributes.gender"
              }
           },
           {
             $group : {
                _id : { generation: "$generation", gender: "$gender"},
                average: { $avg: "$hapiness" }
             }
           }
        ], function(err, result){
            if(err) res.status(500).send({message: "Error getting statistics"});

            statistics.genderGenerationHappiness = result;

            Feedback.aggregate([
                {
                    $project: {    
                        "generation": {
                            $concat: [
                                { $cond: [{$and:[ {$gt:["$faceAttributes.age", 9 ]}, {$lte: ["$faceAttributes.age", 17]}]}, "Generacion Z", ""] },
                                { $cond: [{$and:[ {$gt:["$faceAttributes.age",17]}, {$lte:["$faceAttributes.age", 27]}]}, "Milenials", ""]},
                                { $cond: [{$and:[ {$gt:["$faceAttributes.age",27]}, {$lte:["$faceAttributes.age", 37]}]}, "Generacion Y", ""]},
                                { $cond: [{$and:[ {$gt:["$faceAttributes.age",37]}, {$lt:["$faceAttributes.age", 52]}]}, "Generacion X", ""]},
                                { $cond: [{$and:[ {$gt:["$faceAttributes.age",52]}]}, "Baby Boomer", ""]}
                            ]
                        }  
                    }    
                },
                {
                    $group: { 
                        "_id" : "$generation", 
                        qty: { 
                            $sum: 1
                        } 
                    }
                }], function(err, result){
                    if(err) res.status(500).send({message: "Error getting statistics"});

                    statistics.clientsGeneration = result;

                    Feedback.aggregate(
                        [ { $group:
                            { _id: "$faceAttributes.gender",
                                total: { $sum: 1 } }
                            } 
                        ], function (err, result){
                            if(err) res.status(500).send({message: "Error getting statistics"});
                            
                            statistics.clientsGender = result;

                            Feedback.aggregate(
                                [
                                   {
                                      $project: {
                                         _id: 0,
                                         generation: { $concat: [
                                                  { $cond: [{$and:[ {$gt:["$faceAttributes.age", 9 ]}, {$lte: ["$faceAttributes.age", 17]}]}, "Generacion Z", ""] },
                                                  { $cond: [{$and:[ {$gt:["$faceAttributes.age",17]}, {$lte:["$faceAttributes.age", 27]}]}, "Milenials", ""]},
                                                  { $cond: [{$and:[ {$gt:["$faceAttributes.age",27]}, {$lte:["$faceAttributes.age", 37]}]}, "Generacion Y", ""]},
                                                  { $cond: [{$and:[ {$gt:["$faceAttributes.age",37]}, {$lt:["$faceAttributes.age", 52]}]}, "Generacion X", ""]},
                                                  { $cond: [{$and:[ {$gt:["$faceAttributes.age",52]}]}, "Baby Boomer", ""]}]},
                                         gender: "$faceAttributes.gender"
                                      }
                                   },
                                   {
                                     $group : {
                                        _id : { generation: "$generation", gender: "$gender"},
                                        qty : { $sum: 1 }
                                     }
                                   }
                                ], function(err, result){
                                    if(err) res.status(500).send({message: "Error getting statistics"});

                                    statistics.generationGender = result;

                                    res.json(statistics);
                                }
                             )   
                        }
                    )

                })


        }
    )    
   
}