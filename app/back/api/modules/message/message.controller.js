var nodemailer = require('nodemailer');

module.exports = {
    message: sendMessage
  };

function sendMessage (req, res){
    var channel = req.swagger.params.channel.value.channel;
    var message = req.swagger.params.channel.value.message;
    var receiver = req.swagger.params.channel.value.receiver;

    switch(channel){
        case "MAIL" :
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: process.env.USER_MAIL,
                    pass: process.env.PASS_MAIL
                }
            });
            var mailOptions = {
                from: process.env.USER_MAIL,
                to: receiver,
                subject: 'Correo desde tu cuenta de unisaldo',
                text: message
            };
            transporter.sendMail(mailOptions, function(error, info){  
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
            res.send({message:"Email sent"});
            break
        case "SMS" :
            break
    }
    
}
