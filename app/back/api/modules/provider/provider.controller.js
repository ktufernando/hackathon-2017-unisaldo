'use strict';

/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

const providerStrategies = require('./strategy/providerStrategies');

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var mongoose = require('mongoose'),
    userSchema = require('../user/user.model'),    
    User = mongoose.model('User'),
    request = require('request');

module.exports = {
  addProvider: addProvider,
  selectProviderStrategy: selectProviderStrategy,
  changeStatus: changeStatus
};
    
/*
 |--------------------------------------------------------------------------
 | Add Provider gateway
 |--------------------------------------------------------------------------
 */
function addProvider (req, res) {

  
  let providerStrategy = selectProviderStrategy(req.body.provider);
  
  providerStrategy.getToken(req.body.code,
    req.body.redirectURI,  function(err, result){
      if(err){
        return res.status(500).send({message: err.message});      
      }

      let user=req.user;
      var arr = user.providers || [];
    
      let provider = {
        //Step 1. commond propierties
        providerType  : req.body.provider,
        accessToken : result.access_token,
        providerUserId : result.user_id
      };
    
      var found = false;
      for(var i = 0; i< arr.length ; i++){
        
        
        if(arr[i].providerType.toString().trim() === provider.providerType.toString().trim() ){
           //Lo encontre hago update
           arr[i].accessToken = JSON.parse(result).access_token;
           arr[i].providerUserId = JSON.parse(result).user_id;    
           found =  true;
           break;
        }

      };

      if (!found){
        //No lo encontre, agrego
        arr.push(provider);
        user.providers = arr;
      };

      User.findOneAndUpdate({_id:user._id},user, function(err) {
        if(err){
          return res.status(500).send({message: err.message});
        }
        return res.send({message: 'OK'});   
      });
    });

};
  
/*
 |--------------------------------------------------------------------------
 | Provider Strategies
 |--------------------------------------------------------------------------
 */
function selectProviderStrategy  (strategyCode) {
  let strategies = Object.keys(providerStrategies);
  let strategy="";
  for (let i = 0; i < strategies.length; i++) {
    let strategy = providerStrategies[strategies[i]];
    if (strategy.useStrategy(strategyCode)) {
       return strategy;
    }
  }
}

/*
 |--------------------------------------------------------------------------
 | Enable or disable the provider 
 |--------------------------------------------------------------------------
 */
function changeStatus (req, res){

  var providerType = req.swagger.params.provider.value.providerType;
  var enabled = req.swagger.params.provider.value.enabled;
  var user = req.user;
  var providers = user.providers;

  if(providers){
    var found = false;
    for(var i = 0; i< providers.length ; i++){      
      
      if(providers[i].providerType === providerType ){
         //Lo encontre hago update
         providers[i].enabled = enabled;
         found =  true;
         break;
      }
    }

    User.findOneAndUpdate({_id:user._id},user, function(err) {
      if(err){
        return res.status(500).send({message: err.message});
      }
      return res.send({message: 'OK'});   
    });

  }else{
    res.status(500).send({message: "The user has not providers"});
  }

}
