'use strict';

var request = require('request'),
   util = require('util'),
   _ = require('lodash');


class MarketBBVAProviderStrategy {

   getToken(code, redirectURI, next) {
      // var accessTokenUrl = process.env.BBVA_ACCESS_TOKEN_URI;
      // TOCHECK: no la estas tomando en ubuntu, en el seteo de environment.
      var accessTokenUrl = 'https://connect.bbva.com/token?grant_type=authorization_code&code=%s&redirect_uri=%s';

      console.log(accessTokenUrl);
      var auth = 'Basic ' + new Buffer(process.env.BBVA_CLIENT_ID + ':' + process.env.BBVA_CLIENT_SECRET).toString('base64');
      var options = {
         url: util.format(accessTokenUrl, code, redirectURI),
         headers: {
            'Content-Type': 'application/json',
            'Authorization': auth
         },
      }
      console.log(JSON.stringify(options));
      // Step 1. Exchange authorization code for access token.
      request.post(options, function (err, response, accessToken) {
         console.log("response.statusCode: " + response.statusCode);
         if (err) {
            return next(new Error({
               message: err.message
            }));
         }
         if (response.statusCode !== 200) {
            return next(new Error({
               message: 'Unauthorized'
            }));
         }
         console.log(accessToken);

         //Otro requet GET a https://apis.bbva.com/accounts-sbx/v1/me/accounts
         //Headers
         //Content-Type: application/jsn
         //Authorization: jwt YOURTOKEN
         var userInfoUrl = process.env.BBVA_API_URL + process.env.BBVA_API_COSTUMER;
         var authInfo = 'jwt ' + JSON.parse(accessToken).access_token;
         var optionsUser = {
            url: userInfoUrl,
            headers: {
               'Content-Type': 'application/json',
               'Authorization': authInfo
            }

         }
         console.log(JSON.stringify(optionsUser));
         request.get(optionsUser, function (err, responseUser, userInfo) {
            console.log("responseUser.statusCode: " + responseUser.statusCode);
            if (err) {
               return next(new Error({
                  message: err.message
               }));
            }
            if (response.statusCode !== 200) {
               return next(new Error({
                  message: 'Unauthorized'
               }));
            }
            console.log(userInfo);
            var userData = JSON.parse(userInfo);
            userData.access_token = JSON.parse(accessToken).access_token;
            userData.user_id = userData.userId;

            return next(null, userData);


         });


      });

   }

   getBalance(userId, token, next) {
      // Get Balance from gateway
      var apiUriBalance = process.env.BBVA_API_URI_USERINFO;

      var options = {
         url: apiUriBalance,
         headers: {
            'Accept': 'application/json',
            'Authorization': 'jwt ' + token
         },

      }

      // Step 1. Exchange authorization code for access token.
      request.get(options, function (err, response, body) {
         if (err) {
            return next(new Error({
               message: err.message
            }));
         }
         if (response.statusCode !== 200) {
            return next(new Error({
               message: 'Mercado Libre not work'
            }));
         }

         var json = JSON.parse(body);

         var accounts = json.data.accounts;
         var totals = [];

         var currencies = _.groupBy(accounts, 'currency');

         for (var key in currencies) {
            var o = {
               currency: key,
               amount: 0
            }
            for (var i in currencies[key]) {
               o.amount = o.amount + currencies[key][i].balance
            }
            totals.push(o);
         }

         var obj = {
            totals: totals,
            accounts: accounts
         };

         console.log(obj);

         return next(null, obj);


      });
   }

   useStrategy(strategy) {
      return strategy === 'BBVA';
   }

   getDescription() {
      return 'BBVA';
   }

}


MarketBBVAProviderStrategy.prototype.enable_ = false;

/**
 * Gets the radius.
 * @return {?Boolean}
 */
MarketBBVAProviderStrategy.prototype.isEnable = function () {
   return this.enable_;
};

/**
 * Sets the radius.
 * @param {?Boolean} radius
 */
MarketBBVAProviderStrategy.prototype.setEnable = function (enable) {
   this.enable_ = enable;
};

let marketBBVAProviderStrategy = new MarketBBVAProviderStrategy();
module.exports = marketBBVAProviderStrategy;