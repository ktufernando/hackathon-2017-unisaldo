'use strict';

var  request = require('request'),
      util = require('util');

//var MP = require ("mercadopago");
//var mp = new MP (process.env.MP_CLIENT_ID, process.env.MP_CLIENT_SECRET);


class MercadoPagoProviderStrategy {

getToken(code,redirectURI,next) {
    var accessTokenUrl = process.env.MP_ACCESS_TOKEN_URI;
    var options = {
      url: accessTokenUrl,
      headers : {
          'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: JSON.stringify({
        code: code,
        client_secret: process.env.MP_CLIENT_SECRET,
        grant_type: 'client_credentials',
        client_id: process.env.MP_CLIENT_ID
      })
    }
    
    // Step 1. Exchange authorization code for access token.
    request.post(options, function(err, response, accessToken) {
      if(err) return next(new Error({ message: err.message }));

      if (response.statusCode !== 200) {
        return next(new Error({ message: err.message }));
      }

      return next(null, JSON.parse(accessToken));


    });
    
  }

 getBalance(userId,token,next){
  // Get Balance from gateway
  var apiUriBalance = util.format(process.env.MP_API_URI_BALANCE,userId);
  var params = {
    access_token: token
  };

  // Step 1. Exchange authorization code for access token.
  request.get({ url: apiUriBalance, qs: params, json: true }, function(err, response, balance) {
    if (err) {
      return next(new Error({ message: err.message }));
    }
    if (response.statusCode !== 200) {
      return next(new Error({ message: 'Mercado Libre not work' }));
    }

    var json = balance;
    var obj = {
      totals: [{ 
        currency: balance.currency_id,
        amount: balance.available_balance
      }],
      accounts: []
    };
    
    return next(null, obj);


  });

}

  useStrategy(strategy) {
    return strategy === 'MP';
  }

  getDescription (){
    return 'Mercado Pago';
  }
}


MercadoPagoProviderStrategy.prototype.enable_ = false;

/**
 * Gets the radius.
 * @return {?Boolean}
 */
MercadoPagoProviderStrategy.prototype.isEnable = function() {
  return this.enable_;
};

/**
 * Sets the radius.
 * @param {?Boolean} radius
 */
MercadoPagoProviderStrategy.prototype.setEnable = function(enable) {
  this.enable_ = enable;
};


let mercadoPagoProviderStrategy = new MercadoPagoProviderStrategy();
module.exports = mercadoPagoProviderStrategy;