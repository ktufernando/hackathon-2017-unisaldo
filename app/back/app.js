'use strict';

var SwaggerExpress = require('swagger-express-mw');
var path = require('path');
var cors = require('cors');
var express = require('express');
var logger = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var https = require('https');
var user = require('./api/modules/user/user.controller');
var mail = require('./api/modules/message/message.controller');
var router = express.Router();
var staticRoot = path.join(__dirname, 'public/');

/*var https_options = {
  key: fs.readFileSync('./ssl/server.key'),
  cert: fs.readFileSync('./ssl/server.crt'),
  ca: fs.readFileSync('./ssl/ca.crt'),
  requestCert: true,
  rejectUnauthorized: false
};*/

var app = express();

module.exports = app; // for testing

var config = {
   appRoot: __dirname, // required config
   swaggerSecurityHandlers: {
      Bearer: function (req, scope, next, cb) {
         user.ensureAuthenticated(req, scope, function () {
            //console.log("Authenticated");
            cb();
         });

      }
   }
};


//console.info("NODE_ENV=" + process.env.NODE_ENV);
//if (!process.env.NODE_ENV || process.env.NODE_ENV.toUpperCase() !== 'PRODUCTION') {
   require('env2')('./config/.env');
//}

var PORT = process.env.BACK_PORT;
var HOST = process.env.HOST;

//console.log('Connecting to Mongo. MONGO_URI='+process.env.MONGO_URI);
mongoose.connect(process.env.MONGO_URI,{
  useMongoClient: true
}).then(
  ()  => { 
     console.log('Successful mongodb connection') 
     console.log(process.env.MONGO_URI) 
},
   err => {
      console.error('Error: Could not connect to MongoDB.')
      console.log(process.env.MONGO_URI)
   }
);

SwaggerExpress.create(config, function (err, swaggerExpress) {
   if (err) {
      throw err;
   }

   app.use(cors());

   app.use(logger('dev'));
   app.use(bodyParser.json());
   app.use(bodyParser.urlencoded({
      extended: true
   }));

   swaggerExpress.register(app);

   //console.log('Creating back server on '+HOST+':'+PORT);

   /*var server = https.createServer(https_options, app);
  
   server.on('error', function (e) {
     console.error(e);
   });
  
   server.listen(PORT, HOST, (err)=>{
     //console.log('HTTPS Server listening on %s:%s', HOST, PORT);
   });*/

   app.listen(PORT, (err) => {
      //console.log('HTTP Server listening on port %s', PORT);
   });

   //expose public dir for swagger-ui
   app.use(express.static(staticRoot));
   
   router.use('/', express.static('app', { redirect: false }));
   // rewrite virtual urls to angular app to enable refreshing of internal pages
   router.get('*', function (req, res, next) {
     res.sendFile(path.resolve('app/index.html'));
   });

   module.exports = router;

   app.use(function (req, res, next) {

    // if the request is not html then move along
    var accept = req.accepts('html', 'json', 'xml');
    if (accept !== 'html') {
      return next();
    }
  
    // if the request has a '.' assume that it's for a file, move along
    var ext = path.extname(req.path);
    if (ext !== '') {
      return next();
    }
  
    fs.createReadStream(staticRoot + 'index.html').pipe(res);
  
  });


});
