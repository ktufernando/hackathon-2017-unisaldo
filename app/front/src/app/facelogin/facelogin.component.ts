import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AlertService, AuthService, FaceService } from '../shared';
import { environment } from '../../environments/environment';
import { EmailValidator } from '@angular/forms';
 
@Component({
    selector: 'app-facelogin',
    templateUrl: './facelogin.component.html',
    styleUrls: ['./facelogin.component.scss'],
    animations: [routerTransition()]
})

export class FaceLoginComponent {
    
    loading = false;
    @ViewChild("fileInput") fileInput;
    
    model: any = {

    };

    constructor(private router: Router, 
        public authService: AuthService, 
        private alertService: AlertService,
        private faceService: FaceService) {
    }

    login() {
        const that = this;
        this.loading = true;
        
        let fileBrowser = this.fileInput.nativeElement;
        if (fileBrowser.files && fileBrowser.files[0]) {

            this.faceService.rotateImageO(fileBrowser.files[0]).subscribe(
                data => {
                    this.faceService.resizeImageO(data, 500).subscribe(
                        data => {
                            const formData = new FormData();
                            formData.append("file", data);
                            this.faceService.getAzureFaceId(formData).subscribe(
                                res => {
                                    this.authService.faceLogin(this.model.password, res.faceId)
                                    .subscribe(
                                        data => {
                                            this.router.navigate(['/dashboard']);
                                        },
                                        error => {
                                            this.loading = false;
                                            this.alertService.error(error.json().message);
                                        });
                                },
                                error => {
                                    this.loading = false;
                                    this.alertService.error(error.json().message);
                                });
                        },
                        error => {
                            this.loading = false;
                            this.alertService.error(error.message);
                        });
                },
                error => {
                    this.loading = false;
                    this.alertService.error(error.message);
                });
            
        }
    }

}
