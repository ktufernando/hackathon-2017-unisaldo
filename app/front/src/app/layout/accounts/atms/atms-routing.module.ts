import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtmsComponent } from './atms.component';

const routes: Routes = [
    { path: '', component: AtmsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtmsRoutingModule { }