import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { AlertService, AtmsService } from '../../../shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
   selector: 'app-atms',
   styles: [`
   agm-map {
     height: 420px;
   }
  `],
   templateUrl: './atms.component.html',
   animations: [routerTransition()]
})

export class AtmsComponent implements OnInit {

   atms: any = [];
   loading=false;
   atm = null;

   // Position in Madrid
   lat: number = 40.432643;
   lng: number = -3.700147;
   rad: number = 10; // km

   constructor(private atmsService: AtmsService,
      private alertService: AlertService,
      private modalService: NgbModal
   ) { }

   ngOnInit() {
      console.log(this.atms);
      console.log(this.getAtms());
   }

   getAtms() {
      this.atmsService.atms(this.lat, this.lng, this.rad).subscribe(
         atms => {
            this.atms = atms;
            this.atm = this.atms[0];
            this.loading=true;
         },
         err => {
          this.atms = null;
          this.loading=true;

          console.error(err);
         });
   }

   updateDiv() {
      console.log('check');
   }

   onMarkerClick($event) {
      console.log('check custom');
   }

}
