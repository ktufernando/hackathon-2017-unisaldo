import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AtmsRoutingModule } from './atms-routing.module';
import { AtmsComponent } from './atms.component';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        AtmsRoutingModule,
        PageHeaderModule,
        TranslateModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
         apiKey: `${environment.GOOGLE_API_KEY}`
     }),
 ],
    declarations: [AtmsComponent],
    providers: [
    ]
})
export class AtmsModule { }

