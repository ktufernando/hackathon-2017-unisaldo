import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { AlertService, ProviderService, NotificationService, UserService } from '../../../shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-todos',
    templateUrl: './balances.component.html',
    styleUrls: ['./balances.component.scss'],
    animations: [routerTransition()]
})

export class BalancesComponent implements OnInit {
    
    balance: any = {};
    email: String = null;
    loading= false;

    constructor(private providerService: ProviderService,
                private alertService: AlertService, 
                private modalService: NgbModal,
                private notificationService: NotificationService,
                private userService: UserService
    ) {}

    ngOnInit() {
        this.getUserBalances();
        this.getProfile();
    }

    getUserBalances() {
        this.providerService.balances().subscribe(
            balances => {
                this.balance = balances;
                this.loading=true;  
            } , 
            err => {
                console.error(err);
            });
    }

    getProfile() {
        this.userService.get().subscribe(
            res => {
                console.log(res);
                this.email = res.email;
            }, 
            err => {
                console.log(err);
            });
    }

    sendNotification(){
        var body = 'Total Balances: ';
        for(var i in this.balance.totals){
            body = body + this.balance.totals[i].currency; + ': '
            body = body + this.balance.totals[i].amount + '. ';
        }
        var message = {
            channel: 'MAIL',
            receiver: this.email,
            message: body
        };
        this.notificationService.send(message).subscribe(
            resp => {
                this.alertService.success("Message sent");
            }, 
            err => {
                console.error(err);
                this.alertService.error(err.message);
            });
    }

   
}
