import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { UserService, AlertService, ProviderService } from '../../../shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { PageHeaderModule } from './../../../shared'; 
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-providers',
    templateUrl: './providers.component.html',
    styleUrls: ['./providers.component.scss'],
    animations: [routerTransition()]
})
export class ProvidersComponent implements OnInit {
    
    mpLogin=true;
    mpEnabled=false;
    bbvaLogin=false;
    bbvaEnabled=false;
    profile: any = {};
    mp = null;
    bbva = null;
    loading = false;
    private providerConfig = environment.provider_config;
    
    constructor(private userService: UserService, 
                private alertService: AlertService,
                private router : Router,
                public providerService: ProviderService

    ) {}

    ngOnInit() {
        this.getProfile();
        this.providerService.obsAccessToken
        this.providerService.obsAccessToken.subscribe(provider => {
            this.getProfile();
        });
    }

    providerPermission(provider){
        this.providerService.auth(provider,this.providerConfig);
        console.log(this.providerConfig);
    }

    getProfile() {
        this.userService.get().subscribe(
            res => {
                console.log(res);
                this.profile = res;
                this.profile.providers.forEach(element => {
                    if(element.providerType === 'MP'){
                        this.mp = element;
                    }else if (element.providerType === 'BBVA'){
                        this.bbva = element;
                    }
                });
            }, 
            err => {
                console.log(err);
            });
    }

    onEnabledMp(checked){
        this.update({providerType: 'MP', enabled: checked});
    }
    onEnabledBbva(checked){
        this.update({providerType: 'BBVA', enabled: checked});
    }

    update(o) {
        this.providerService.enabledProvider(o).subscribe(
            response => {
                console.log(response);
                this.alertService.success("Provider Updated");
            }, 
            err => {
                console.log(err);
                this.alertService.error(err.json().message);
            });
    }
}
