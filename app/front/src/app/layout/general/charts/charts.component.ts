import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ChartsService } from './charts.service';
import { AlertService } from '../../../shared';

@Component({
    selector: 'app-charts',
    templateUrl: './charts.component.html',
    styleUrls: ['./charts.component.scss'],
    animations: [routerTransition()]
})
export class ChartsComponent implements OnInit {

    constructor(private chartsService: ChartsService,
                private alertService: AlertService
     ) { }

    ngOnInit() {
        this.chartsService.get().subscribe(
            response => {
               //usa este response
            },
            err => {
               console.error(err);
            });
    }

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barChartLabels: string[] = ['Baby Bombers', 'Generacion X', 'Generacion Y', 'Millennials', 'Generacion Z'];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;

    public barChartData: any[] = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Mujeres' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Hombres' }
    ];
    // Doughnut
    public doughnutChartLabels: string[] = ['Baby Bombers', 'Generacion X', 'Generacion Y', 'Millennials', 'Generacion Z'];
    public doughnutChartData: number[] = [350, 250, 100,150,80];
    public doughnutChartType: string = 'doughnut';
    // Radar
    public radarChartLabels: string[] = ['Baby Bombers', 'Generacion X', 'Generacion Y', 'Millennials', 'Generacion Z'];
    public radarChartData: any = [
        { data: [65, 59, 90, 81, 56], label: 'Mujeres' },
        { data: [28, 48, 40, 19, 96], label: 'Hombres' }
    ];
    public radarChartType: string = 'radar';
    // Pie
    public pieChartLabels: string[] = ['Mujeres', 'Hombres'];
    public pieChartData: number[] = [300, 700];
    public pieChartType: string = 'pie';
    // PolarArea
    public polarAreaChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
    public polarAreaChartData: number[] = [300, 500, 100, 40, 120];
    public polarAreaLegend: boolean = true;

    public polarAreaChartType: string = 'polarArea';
    // lineChart
    public lineChartData: Array<any> = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
        { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' }
    ];
    public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions: any = {
        responsive: true
    };
    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // //console.log(e);
    }

    public chartHovered(e: any): void {
        // //console.log(e);
    }

    public randomize(): void {
        // Only Change 3 values
        const data = [
            Math.round(Math.random() * 100),
            59,
            80,
            (Math.random() * 100),
            56,
            (Math.random() * 100),
            40
        ];
        const clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        this.barChartData = clone;
        /**
         * (My guess), for Angular to recognize the change in the dataset
         * it has to change the dataset variable directly,
         * so one way around it, is to clone the data, change it and then
         * assign it;
         */
    }

    
}
