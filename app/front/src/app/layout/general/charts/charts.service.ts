import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Observable';
import {environment} from '../../../../environments/environment';


@Injectable()
export class ChartsService { 

    backend = `${environment.domain_back}`;
    
    constructor(private _http: InterceptorService) {}

    get() : Observable<any>{
        return this._http.get(`${this.backend}/api/private/face/feedback`)
        .map((response: Response) => response.json())
        .catch((error:any) => Observable.throw(error || 'Server error'));
    }
 
   
}