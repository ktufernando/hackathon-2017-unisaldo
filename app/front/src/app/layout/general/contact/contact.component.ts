import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { AlertService, NotificationService, UserService } from '../../../shared';

@Component({
    selector: 'app-todos',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    animations: [routerTransition()]
})

export class ContactComponent {
    
    model: any = {};
    loading = false;

    constructor(private notificationService: NotificationService, 
                private alertService: AlertService,
                private userService: UserService
    ) {}

    sendNotification(){
        
        var message = {
            channel: 'MAIL',
            receiver: 'fervaldes11@gmail.com',
            message: this.model.message
        };
        this.notificationService.send(message).subscribe(
            resp => {
                this.alertService.success("Message sent");
                this.loading = false;
            }, 
            err => {
                console.error(err);
                this.alertService.error(err.message);
                this.loading = false;
            });
    }
    
}
