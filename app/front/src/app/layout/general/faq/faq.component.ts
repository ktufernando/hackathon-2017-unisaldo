import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
    animations: [routerTransition()]
})
export class FaqComponent implements OnInit {
    // bar Faq
    public barFaqOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barFaqLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    public barFaqType: string = 'bar';
    public barFaqLegend: boolean = true;

    public barFaqData: any[] = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
    ];
    // Doughnut
    public doughnutFaqLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
    public doughnutFaqData: number[] = [350, 450, 100];
    public doughnutFaqType: string = 'doughnut';
    // Radar
    public radarFaqLabels: string[] = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];
    public radarFaqData: any = [
        { data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A' },
        { data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B' }
    ];
    public radarFaqType: string = 'radar';
    // Pie
    public pieFaqLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
    public pieFaqData: number[] = [300, 500, 100];
    public pieFaqType: string = 'pie';
    // PolarArea
    public polarAreaFaqLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
    public polarAreaFaqData: number[] = [300, 500, 100, 40, 120];
    public polarAreaLegend: boolean = true;

    public polarAreaFaqType: string = 'polarArea';
    // lineFaq
    public lineFaqData: Array<any> = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
        { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' }
    ];
    public lineFaqLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineFaqOptions: any = {
        responsive: true
    };
    public lineFaqColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineFaqLegend: boolean = true;
    public lineFaqType: string = 'line';

    // events
    public faqClicked(e: any): void {
        // //console.log(e);
    }

    public faqHovered(e: any): void {
        // //console.log(e);
    }

    public randomize(): void {
        // Only Change 3 values
        const data = [
            Math.round(Math.random() * 100),
            59,
            80,
            (Math.random() * 100),
            56,
            (Math.random() * 100),
            40
        ];
        const clone = JSON.parse(JSON.stringify(this.barFaqData));
        clone[0].data = data;
        this.barFaqData = clone;
        /**
         * (My guess), for Angular to recognize the change in the dataset
         * it has to change the dataset variable directly,
         * so one way around it, is to clone the data, change it and then
         * assign it;
         */
    }

    constructor() {
    }

    ngOnInit() {
    }
}
