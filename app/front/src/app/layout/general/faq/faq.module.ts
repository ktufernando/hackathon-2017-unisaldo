import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Faq } from 'ng2-charts';

import { FaqRoutingModule } from './faq-routing.module';
import { FaqComponent } from './faq.component';
import { PageHeaderModule } from '../../../shared';

@NgModule({
    imports: [
        CommonModule,
        Ng2Faq,
        FaqRoutingModule,
        PageHeaderModule
    ],
    declarations: [FaqComponent]
})
export class FaqModule { }
