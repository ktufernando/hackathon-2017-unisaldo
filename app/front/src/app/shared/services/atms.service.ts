import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { InterceptorService } from 'ng2-interceptors';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class AtmsService {

   private backend = `${environment.domain_back}`;

   constructor(private _http: InterceptorService) { }

   atms(lat, lng, rad): Observable<any> {

      var params = '?latitude='+lat+'&longitude='+lng+'&radius='+rad;
      var call = `${this.backend}/api/private/atms` + params;
      console.log(call);

      return this._http.get(call)
         .map((response: Response) => response.json())
         .catch((error: any) => Observable.throw(error || 'Server error'));
   }

}