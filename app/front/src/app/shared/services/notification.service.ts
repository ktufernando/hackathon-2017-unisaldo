import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { InterceptorService } from 'ng2-interceptors';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Router, NavigationStart } from '@angular/router';
 
@Injectable()
export class NotificationService {

    private backend = `${environment.domain_back}`;
    
    constructor(private _http: InterceptorService) { 
        
    }
 
    send(notification) : Observable<any> {
        // ...using get request
        return this._http.post(`${this.backend}/api/private/message`,notification)
            // ...and calling .json() on the response to return data
            .map((res:Response) => 
                res.json()
            )
            //...errors if any
            .catch((error:any) => Observable.throw(error || 'Server error'))
            ;
    }


}