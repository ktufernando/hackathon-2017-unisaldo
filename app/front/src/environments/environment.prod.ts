export const environment = {
   production: true,
   domain_front: 'https://ristretto.anycloud.info',
   domain_back: 'https://ristretto.anycloud.info',
   recaptcha: '6Le4cEIUAAAAAHaH4DIL_Z43-e6nOQN3qIRCNMsk',
   GOOGLE_API_KEY: 'AIzaSyCv0fC-mxSrKQBW059IQ2zqA5aEv4DIJ88',
   provider_config: {
      'accessTokenUri': '/api/private/provider',
      'redirectURI': '/accounts/providers',
      "MP": {
         'href': 'https://auth.mercadopago.com.ar/authorization?client_id=1220087716559900&response_type=code&platform_id=mp&redirect_uri=https://ristretto.anycloud.info/accounts/providers'
      },
      "BBVA": {
         'href': 'https://connect.bbva.com/sandboxconnect?client_id=app.bbva.ristreto&response_type=code&redirect_uri=https://ristretto.anycloud.info/accounts/providers'
      }
   }
}

