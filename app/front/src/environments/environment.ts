export const environment = {
   production: false,

   domain_front: 'http://localhost:4200',
   //domain_back: 'http://localhost:5000',
   domain_back: 'https://ristretto.anycloud.info',
   recaptcha: '6Le4cEIUAAAAAHaH4DIL_Z43-e6nOQN3qIRCNMsk',
   GOOGLE_API_KEY: 'AIzaSyCv0fC-mxSrKQBW059IQ2zqA5aEv4DIJ88',
   provider_config: {
      'accessTokenUri': '/api/private/provider',
      'redirectURI': '/accounts/providers',
      "MP": {
         'href': 'https://auth.mercadopago.com.ar/authorization?client_id=1381080099338534&response_type=code&platform_id=mp&redirect_uri=http://localhost:4200/accounts/providers'
      },
      "BBVA": {
         'href': 'https://connect.bbva.com/sandboxconnect?client_id=app.bbva.ristreto&response_type=code&redirect_uri=http://localhost:4200/accounts/providers'
      }
   }
}

