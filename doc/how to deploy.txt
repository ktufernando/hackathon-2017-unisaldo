0. variables

MONGO_URI=mongodb://127.0.0.1:27017/test
HOST=127.0.0.1
BACK_PORT=5000
FRONT_PORT=4200
NODE_ENV=Production
GOOGLE_SECRET=eBlvWSxHF0FpmpcTwLkvKorp
FACEBOOK_SECRET=ff3b8afa1e3f05be9ec9e935e7d4503a
TOKEN_SECRET=s3cr3t
AZURE_SUBSCRIPTION_KEY=66dd523dbefb47d78d85e434b13262a9
AZURE_DETECT_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect
AZURE_VERIFY_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/verify
RECAPTCHA=6LcjhzUUAAAAAP3eeFePBpOSSCeQoQ1lDwH8QHcN
AZURE_PERSON_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/%s/persons
AZURE_PERSIST_FACE_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/%s/persons/%s/persistedFaces
AZURE_IDENTIFY_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/identify
AZURE_TRAIN_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/%s/train
GOOGLE_API_KEY=AIzaSyDUj0NuPPUfMrBNDBglkeeZy4oo9x6cR_8
GOOGLE_MAPS_HOST=https://maps.googleapis.com/maps/api/geocode/json
MP_SHORT_NAME=ristretto
MP_CLIENT_ID=4005600052108796
MP_CLIENT_SECRET=5nmNYIqMJnQlqYN63i3zmZpKeSSB2RGE
MP_ACCESS_TOKEN=TEST-5532061874310277-101110-0404445e2106ca2c79591b2a64163922__LB_LD__-8223153
AZURE_IDENTIFY_URI=https://westcentralus.api.cognitive.microsoft.com/face/v1.0/identify
MP_ACCESS_TOKEN_URI=https://api.mercadopago.com/oauth/token
MP_API_URI_BALANCE=https://api.mercadopago.com/users/%s/mercadopago_account/balance
BBVA_ACCESS_TOKEN_URI=https://connect.bbva.com/token?grant_type=authorization_code&code=%s&redirect_uri=%s
BBVA_CLIENT_ID=app.bbva.ristreto
BBVA_CLIENT_SECRET=1Rf@rXmtPxvtMOBPy8ffjqd2oWKULqqUOKK%LJJy3%2Quc5lgO@hb@NLk@Zk%pSs
BBVA_API_URL=https://apis.bbva.com
BBVA_API_ATM_PATH=/payments-sbx/v1/atms
BBVA_API_URI_USERINFO=https://apis.bbva.com/accounts-sbx/v1/me/accounts
BBVA_API_COSTUMER=/customers-sbx/v1/me-basic
USER_MAIL=ristretto.ninja.team@gmail.com
PASS_MAIL=ScoobyDooWasCrazy

1. Obtener nombre de PC de aws
      diego: ec2-18-231-81-16.sa-east-1.compute.amazonaws.com
      hack: ec2-54-86-244-252.compute-1.amazonaws.com

2. Agregar el host en
    2.1 Consola de google developer
        URL: https://console.developers.google.com/apis
        Editar el cliente de ouath "Ristretto App" y agregar en entrada y salida la url de aws, con puerto.
        Ejeemplo: http://ec2-54-86-244-252.compute-1.amazonaws.com
                  https://ec2-54-86-244-252.compute-1.amazonaws.com
        IMPORTANTE!!!! En las de redirección, va con / FINAl !!!!!!!!!!!
                  http://ec2-54-86-244-252.compute-1.amazonaws.com/
                  https://ec2-54-86-244-252.compute-1.amazonaws.com/

    2.2 Consola de facebook
        URL: https://developers.facebook.com/apps

    2.3 Consola de recaptcha
        URL: https://www.google.com/recaptcha/admin#list


    2.4 Archivo app/front/src/environments/environment.prod.ts

    2.5 Actualizar el host en las variables de scripts/*.sh 

3. Agregar el usuario admin en mongodb
        {
        "picture":"/assets/images/flat-avatar.png",
        "displayName":"admin",
        "email":"admin@admin.com",
        "password":"$2a$10$doX.GZphql/iONQrKIVVvucCjeI0vRZxos5.A7GJFbn2dHgBzaJPe",
        "face":null,
        "roles":["user","admin"]
        }

        El email es "admin@admin.com" y el password es "admin".
        Hay que agregar ese user al mongo de prod y después loguiarse con el para poder eliminar/modificar otros.

4. Actualizar la versión en el pie de pagina app/front/src/app/shared/components/footer/footer.component.html

5. Instalar maquina en aws con scripts/install-aws.sh
    Esto instala nginx en aws, jenkins y hace despliegue inicial de la app

6. Entrar a la consola de jenkins (instalado en paso anterior)y configurar job (segun scripts/deploy-jenkins.sh)
   Esto deja el build autom{atico al hacer push a la rama Master.

