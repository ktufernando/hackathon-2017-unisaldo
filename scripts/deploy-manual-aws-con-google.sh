#!/bin/sh
# Script to run in aws machine with repo donwloaded and nginx installed 
cd ~/workspace/unisaldo/
git pull
sudo systemctl restart mongodb
nvm use node
# start back
cd ~/workspace/unisaldo/app/back
forever stopall
npm i --production
forever start app.js 
# deploy front
cd ~/workspace/unisaldo/app/front
tar -zxvf dist.tar.gz 
#npm i --production && ng build --prod --aot && npm run precache && cp -r nodeserver/* dist
sudo cp -R ~/workspace/unisaldo/app/front/dist/. /var/www/html/app
# config nginx 
sudo cp ~/workspace/unisaldo/.circleci/root/etc/nginx/conf.d/default.conf /etc/nginx/sites-available/default
sudo nginx -t
sudo /etc/init.d/nginx reload

