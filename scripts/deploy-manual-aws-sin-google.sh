#!/bin/sh
# Script to run in aws machine with repo donwloaded and nginx installed 
# restart mongo and set nvm
sudo /etc/init.d/nginx stop
sudo systemctl restart mongodb
nvm use node
# download code 
cd ~/workspace/unisaldo
git pull
# start back
cd ~/workspace/unisaldo/app/back
forever stopall
forever stopall
npm i --production
forever start app.js 
forever list
# compile front
cd ~/workspace/unisaldo/app/front
npm i
ng build --prod --aot && npm run precache && cp -r nodeserver/* dist
sudo cp -R ~/workspace/unisaldo/app/front/dist/. /var/www/html/app
# config nginx 
sudo cp ~/workspace/unisaldo/.circleci/root/etc/nginx/conf.d/default.conf /etc/nginx/sites-available/default
sudo nginx -t
sudo /etc/init.d/nginx start
# check log nginx
cat /home/ubuntu/.forever/IGoN.log