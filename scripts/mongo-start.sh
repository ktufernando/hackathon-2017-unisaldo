#delete collection:
db.users.deleteMany({})
db.faces.deleteMany({})

#insert user admin:

db.users.insertMany(
[
{
"picture":"/assets/images/flat-avatar.png",
"displayName":"admin",
"email":"admin@admin.com",
"password":"$2a$10$doX.GZphql/iONQrKIVVvucCjeI0vRZxos5.A7GJFbn2dHgBzaJPe",
"face":null,
"roles":["user","admin"]
}])

